/***************************************************************************
 *   Copyright (C) 2008 by David Santo Orcero, David del Rio Medina        *
 *   irbis@orcero.org, ddrm@alu.uma.es                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PANEL2D_H
#define PANEL2D_H

#include <qwidget.h>
#include <qtimer.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qlabel.h>
#include <qcanvas.h>
#include <qwmatrix.h>

#include "dicomimageview.h"
#include "dicomfformat.h"
#include "dicomimage.h"
#include "Panel2DBase.h"

/**
@author David Santo Orcero
*/
class Panel2D:public Panel2DBase
{
Q_OBJECT public:
    Panel2D (QWidget * parent = 0, const char *name = 0);

   ~Panel2D ();

  void setPixmap (const QPixmap &);
  void setDICOM (DICOMimage * dicomimage);
  void zoomout ();
  void zoomin ();
  float m_zoom;
  DICOMimage *m_dicomimage;


  public slots:void updatePixmap ();

//   void scale();


protected:

    /**
    * A timer that updates the pixmap when timed out
    */
    QTimer m_timer;

    /**
    * The pixmap
    */
  QImage m_pixmap;
  QCanvas *m_canvas;
  QCanvasPixmapArray *m_pixmaparray;
  float m_zoomfactor;

private:
//      void        resizeEvent( QResizeEvent * );
  void draw16BitsGray ();
  void draw8BitsGray ();
  void draw8BitsRGB ();
  void draw16BitsPaletteColor8BitsAlloc ();
  void draw8BitsPaletteColor8BitsAlloc ();
  void draw8BitsPaletteColor16BitsAlloc ();
  void draw16BitsPaletteColor16BitsAlloc ();
  void draw8BitsYBRFull ();
  void YBRFullToRGB (unsigned char Y, unsigned char Cb, unsigned char Cr,
		     unsigned char *r, unsigned char *g, unsigned char *b);
  void drawRLE ();
};

#endif
