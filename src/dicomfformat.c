/***************************************************************************
 *   Copyright (C) 2008 by David del Rio Medina, David Santo Orcero        *
 *   ddrm@alu.uma.es, irbis@orcero.org                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/



/* Dicom file format */

#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "dicomfformat.h"
#include "headDataBase.h"

#define DEFAULT_TRANSFER_SYNTAX "1.2.840.10008.1.2"
#define EXPLICIT_TRANSFER_SYNTAX "1.2.840.10008.1.2.1"
#define BIG_ENDIAN_TRANSFER_SYNTAX "1.2.840.10008.1.2.2"
#define RLE_TRANSFER_SYNTAX "1.2.840.10008.1.2.5"

void
freeDicomImage (DICOMimage * image)
{
  freeDicomHead (image->head);
  if (image->rle)
    {
      free (image->rleImageLocation);
    }
  if (image->jpeg)
    {
      free (image->jpegImageLocation);
    }
  free (image);
  image = NULL;
}

unsigned short
uShortBigEndian (char *paux)
{
  unsigned short actval = 0;
  actval = (unsigned short) *((unsigned char *) (paux));
  actval = actval << 8;
  actval |= (unsigned short) *((unsigned char *) paux + 1);
  actval &= 0xffff;
  return actval;
}

unsigned int
uIntBigEndian (char *paux)
{
  unsigned int a1 = 0;
  unsigned int a2 = 0;
  unsigned int a3 = 0;
  unsigned int a4 = 0;
  unsigned int actval;

  a1 = (unsigned int) *((unsigned char *) (paux));
  a1 = a1 << 24;
  a2 = (unsigned int) *((unsigned char *) (paux + 1));
  a2 = a2 << 16;
  a3 = (unsigned int) *((unsigned char *) (paux + 2));
  a3 = a3 << 8;
  a4 = (unsigned int) *((unsigned char *) (paux + 3));

  actval = a1 + a2 + a3 + a4;
  return actval;
}

unsigned short
getUShort (char *buffer, char bigEndian)
{
  if (bigEndian)
    {
      return (uShortBigEndian (buffer));
    }
  else
    {
      return (*(unsigned short *) buffer);
    }
}

unsigned int
getUInt (char *buffer, char bigEndian)
{
  if (bigEndian)
    {
      return (uIntBigEndian (buffer));
    }
  else
    {
      return (*(unsigned int *) buffer);
    }
}

void
maxMinPix (DICOMimage * image, unsigned short int *maxPix,
	   unsigned short int *minPix)
{
  char *paux;
  unsigned int x, y;
  unsigned short actval;
  paux = (char *) image->image;

  *maxPix = 0;
  *minPix = 65535;

  for (y = 0; y < image->height; y++)
    {
      for (x = 0; x < image->width; x++)
	{
	  actval = getUShort (paux, image->bigEndian);
	  if ((actval >= 32768) && (image->withSign))	/*negatives pixels not allowed */
	    actval = 0;
	  if (actval > *maxPix)
	    *maxPix = actval;
	  if (actval < *minPix)
	    *minPix = actval;
	  /* f�r Ricardo */
	  /* Two incl %eax are faster on older processors    *
	   *  than one addl $2, %eax                         *
	   *  Remember that $2 is stored as a 32 bit number. *
	   *  Deep ignorance of the inners of gcc generated  *
	   *  code and of assembly of 32 bit processors can  *
	   *  not be compensated with a bad faith entry on   *
	   *  your blog.                                     *
	   * I know that it is easier to blog than to study  *
	   * and learn from people that know more than you.  *
	   * Try It. Maybe you like it.                      */
	  paux++;
	  paux++;
	}
    }
}

void
maxMinPixPaletteColor (DICOMimage * image, unsigned short int *maxPix,
		       unsigned short int *minPix)
{
  char *paux;
  unsigned int x;
  unsigned short actval;
  unsigned int length;
  length = image->paletteColorData.entriesNumber;
  *maxPix = 0;
  *minPix = 0;

  paux = (char *) image->paletteColorData.redTable;
  for (x = 0; x < length; x++)
    {
      actval = getUShort (paux, image->bigEndian);
      if ((actval >= 32768) && (image->withSign))	/*negatives pixels not allowed */
	actval = 0;
      if (actval > *maxPix)
	*maxPix = actval;
      if (actval < *minPix)
	*minPix = actval;
      paux++;
      paux++;
    }

  paux = (char *) image->paletteColorData.greenTable;
  for (x = 0; x < length; x++)
    {
      actval = getUShort (paux, image->bigEndian);
      if ((actval >= 32768) && (image->withSign))	/*negatives pixels not allowed */
	actval = 0;
      if (actval > *maxPix)
	*maxPix = actval;
      if (actval < *minPix)
	*minPix = actval;
      paux++;
      paux++;
    }

  paux = (char *) image->paletteColorData.blueTable;
  for (x = 0; x < length; x++)
    {
      actval = getUShort (paux, image->bigEndian);
      if ((actval >= 32768) && (image->withSign))	/*negatives pixels not allowed */
	actval = 0;
      if (actval > *maxPix)
	*maxPix = actval;
      if (actval < *minPix)
	*minPix = actval;
      paux++;
      paux++;
    }
}

/* Check if the loaded file is a DICOM file */

int
isDCM (const char *head)
{
  return (!(strncmp ((char *) (head + 128), "DICM", 4)));
}

unsigned int
getSQsize (char *pointer)
{				/* get size of a sequence (VR=SQ), see document 5, page 40 ot the standard */
  unsigned int value;
  unsigned int size = 0;
  char end = 0;

  while (!end)
    {
      value = *(unsigned short *) pointer;
      if (value == 0xFFFE)
	{
	  pointer++;
	  pointer++;
	  value = *(unsigned short *) pointer;
	  if (value == 0xE0DD)
	    {
	      end = 1;
	      size += 6;	/* two bytes for 0xE0DD, four bytes for 0x0000 */
	    }
	  else
	    {
	      size++;
	      size++;
	    }
	}
      size++;
      size++;
      pointer++;
      pointer++;
    }

  return size;
}

char
isJPEG (char *transferSyntax)
{
  char *transferSyntaxEndDigits = malloc (3);
  size_t transSynSize = strlen (transferSyntax);
  transferSyntaxEndDigits[0] = transferSyntax[transSynSize - 2];
  transferSyntaxEndDigits[1] = transferSyntax[transSynSize - 1];
  transferSyntaxEndDigits[2] = 0;
  int lastDigits = atoi (transferSyntaxEndDigits);
  free (transferSyntaxEndDigits);
  if (((lastDigits >= 50) && (lastDigits <= 56))
      || ((lastDigits >= 59) && (lastDigits <= 64)))
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

char
isRLE (char *transferSyntax)
{
  return (!strcmp (transferSyntax, RLE_TRANSFER_SYNTAX));
}

void
getJPEGdata (char *image, char **jpegImageLocation,
	     unsigned int *jpegImageSize)
{
  unsigned short twoBytes = 0;
  char *pointer = image;
  char *auxPointer;

  twoBytes = uShortBigEndian ((char *) pointer);
  while (twoBytes != 0xFFD8)
    {				/* 0xFFD8 Start Of Image (SOI) mark defined in JPEG standard */
      pointer++;
      twoBytes = uShortBigEndian ((char *) pointer);
    }
  auxPointer = pointer;
  pointer -= 4;			/* Size of JPEG is saved in the 4 bytes before the start of the JPEG image */
  *jpegImageSize = *(unsigned int *) pointer;
  *jpegImageLocation = malloc (*jpegImageSize);
  memcpy (*jpegImageLocation, auxPointer, *jpegImageSize);
}

void
getRLEdata (char *image, char **rleImageLocation, unsigned int *rleImageSize)
{				/* just the first frame */
  char *pointer = image;
  unsigned int fourBytes;

  pointer += 4;
  fourBytes = *(unsigned int *) pointer;	/* length of Basic Offset Table */
  pointer += 4;
  pointer += fourBytes;
  pointer += 4;
  *rleImageSize = *(unsigned int *) pointer;	/* length of the first frame (multi-frame not supported) */
  pointer += 4;
  *rleImageLocation = malloc (*rleImageSize);
  memcpy (*rleImageLocation, pointer, *rleImageSize);	/* now we are in the header of the first frame */
}

char
isExplicit (const char *transferSyntax)
{
  return (strcmp (transferSyntax, DEFAULT_TRANSFER_SYNTAX));
}

char
isBigEndian (const char *transferSyntax)
{
  return (!strcmp (transferSyntax, BIG_ENDIAN_TRANSFER_SYNTAX));
}

char
isSupported (const char *transferSyntax)
{
  if ((strcmp (transferSyntax, EXPLICIT_TRANSFER_SYNTAX))
      && (isExplicit (transferSyntax)) && (!isBigEndian (transferSyntax))
      && (!isJPEG (transferSyntax)) && (!isRLE (transferSyntax)))
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

char
isEspecialVr (const char *vr)
{
  return (vr[0] == 'O') || (!strcmp (vr, "SQ")) || (!strcmp (vr, "UT"))
    || (!strcmp (vr, "UN"));
}

char
isSQ (const char *vr)
{
  return (!strcmp (vr, "SQ"));
}

char
isADicomFile (char *buffer, DICOMimage * image, long int fileSize)
{
  /*
     We exploit the fact that at least the two first data sets of a dicom head have the group tag equal to 8.
   */
  unsigned short group, element;
  unsigned int size;
  char *vr;

  vr = malloc (3);

  /* First code */
  group = getUShort (buffer, image->bigEndian);
  buffer++;
  buffer++;

  if (group != 8)
    {
      return 0;
    }

  /* Next code */
  element = getUShort (buffer, image->bigEndian);
  buffer++;
  buffer++;
  if (image->explicitHead)
    {
      vr[0] = *buffer;
      buffer++;
      vr[1] = *buffer;
      vr[2] = 0;
      buffer++;
      if (isEspecialVr (vr))
	{			/*see document 5 of the standard, pages 36-37 */
	  buffer++;
	  buffer++;
	  size = getUInt (buffer, image->bigEndian);
	  buffer += 4;
	}
      else
	{
	  size = getUShort (buffer, image->bigEndian);
	  buffer++;
	  buffer++;
	}
    }
  else
    {
      size = getUInt (buffer, image->bigEndian);
      buffer += 4;
    }

  /* To avoid the crash of the program if we read memory beyond the memory reserved for the file.
     Think that we could have been checking a non dicom file that begins with a byte equal to 8
     ,  (thinking is the right group tag) and then get a wrong "size" larger than the file size */
  if (size > fileSize)
    {
      return 0;
    }

  buffer += size;
  group = getUShort (buffer, image->bigEndian);
  if (group != 8)
    {
      return 0;
    }

  free (vr);

  return 1;
}

void
renderMetaHeader (const char *head, char **pointerToHead,
		  char **transferSyntax)
{
  char *headPointer;
  unsigned int group, element, dataSize;
  char *vr;

  headPointer = head;

  if (isDCM (headPointer))
    {
      headPointer += 132;	/* jumping preamble (128 bytes) and Dicom Prefix (4 bytes) */
    }

  group = *(unsigned short *) headPointer;
  if (group == 0x0002)
    {				/* the file has Meta-Header */
      vr = malloc (3);
      while (group == 0x0002)
	{
	  headPointer++;
	  headPointer++;
	  element = *(unsigned short *) headPointer;
	  headPointer++;
	  headPointer++;
	  vr[0] = *headPointer;
	  headPointer++;
	  vr[1] = *headPointer;
	  vr[2] = 0;
	  headPointer++;
	  if (isEspecialVr (vr))
	    {			/*see document 5 of the standard, pages 36-37 */
	      headPointer++;
	      headPointer++;
	      dataSize = *(unsigned int *) headPointer;
	      headPointer += 4;
	      headPointer += dataSize;
	    }
	  else
	    {
	      dataSize = *(unsigned short *) headPointer;
	      headPointer++;
	      headPointer++;
	      if (element == 0x0010)
		{		/*getting transfer syntax */
		  char *aux = malloc (sizeof (char) * (dataSize + 1));
		  strncpy (aux, headPointer, dataSize);
		  aux[dataSize] = 0;
		  *transferSyntax = malloc (sizeof (char) * (dataSize + 1));
		  strcpy (*transferSyntax, aux);
		  free (aux);
		}
	      headPointer += dataSize;
	    }
	  group = *(unsigned short *) headPointer;
	}
    }
  else
    {				/* too bad no meta-header, assuming implicit vr little endian transfer syntax */
      *transferSyntax = malloc (18);
      strcpy (*transferSyntax, DEFAULT_TRANSFER_SYNTAX);
    }

  *pointerToHead = headPointer;
}

/* Upload in memory the whole image file */

DICOMimage *
loadImage (const char *filefullpath)
{
  long int filesize;
  char *buffer, *transferSyntax, *bufferWithoutMetaHeader;
  int stream;
  struct stat streamstat;
  DICOMimage *newdicomimage;

#ifdef _DICOM_PARANOID_DEBUG
  printf ("Loading the images.\n");
#endif

  stream = open (filefullpath, O_RDONLY);

  if (stream == -1)
    {
      printf ("fstat: open(\"%s\",O_RDONLY): %s (%i)",
	      filefullpath, strerror (errno), errno);
      fflush (NULL);
      return NULL;
    }

  if (fstat (stream, &streamstat) == -1)
    {
      printf ("fstat: fstat: %s (%i)", strerror (errno), errno);
      fflush (NULL);
      return NULL;
    }


  filesize = streamstat.st_size;
  buffer = malloc (filesize);
  read (stream, buffer, filesize);
  close (stream);
#ifdef _DICOM_PARANOID_DEBUG
  printf ("The file was opened,and I read %ld bytes of the file at %s.\n",
	  filesize, filefullpath);
#endif
  renderMetaHeader (buffer, &bufferWithoutMetaHeader, &transferSyntax);
  if (isSupported (transferSyntax))
    {
      newdicomimage =
	renderDCMhead (bufferWithoutMetaHeader, transferSyntax, filesize);
      free (buffer);
      return newdicomimage;
    }
  else
    {
      return _ERROR_ISNOTSUPPORTED;
    }
}


/* Render the metadata of the file */

DICOMimage *
renderDCMhead (char *buffer, char *transferSyntax, long int fileSize)
{
  char unknownField = 0;
  char *bufferT = buffer;
  int dicomFieldIndex;
  char *value;
  char *vr;
  unsigned short group = 0;
  unsigned short element = 0;
  unsigned int esize;
  DICOMhead actualdicomhead;
  DICOMhead tempdicomhead;
  DICOMimage *actualdicomimage;

  /* there is not a single tag */
  actualdicomhead = NULL;

  /* We prepare the DICOM image */

  actualdicomimage = malloc (sizeof (DICOMimage));
  if (actualdicomimage == NULL)
    {
      printf ("ERROR: Could not allocate enough memory for the structure.");
      fflush (NULL);
      return (NULL);
    }

  actualdicomimage->width = 0;
  actualdicomimage->height = 0;
  actualdicomimage->grayScale = 0;
  actualdicomimage->rgb = 0;
  actualdicomimage->paletteColor = 0;
  actualdicomimage->ybrFull = 0;
  actualdicomimage->jpeg = isJPEG (transferSyntax);
  actualdicomimage->rle = isRLE (transferSyntax);

  actualdicomimage->explicitHead = isExplicit (transferSyntax);
  actualdicomimage->bigEndian = isBigEndian (transferSyntax);

  vr = malloc (3);

  if (!isADicomFile (buffer, actualdicomimage, fileSize))
    {
      return _ERROR_ISNOTDICOM;
    }

  printf ("Transfer Syntax: %s\n", transferSyntax);

  /* While not reached the end of the tags, or the end of the block do... */
  while (!((group == 0x7FE0) && (element == 0x0010)))
    {
#ifdef _DICOM_PARANOID_DEBUG
      printf ("\n----------------------------------------------------\n");
#endif

      vr[0] = "";
      vr[1] = "";
      vr[2] = 0;
      /* First code */
      group = getUShort (bufferT, actualdicomimage->bigEndian);
      bufferT++;
      bufferT++;
      /* Next code */
      element = getUShort (bufferT, actualdicomimage->bigEndian);
      bufferT++;
      bufferT++;
      if (actualdicomimage->explicitHead)
	{
	  vr[0] = *bufferT;
	  bufferT++;
	  vr[1] = *bufferT;
	  vr[2] = 0;
	  bufferT++;
	  if (isEspecialVr (vr))
	    {			/*see document 5 of the standard, pages 36-37 */
	      bufferT++;
	      bufferT++;
	      esize = getUInt (bufferT, actualdicomimage->bigEndian);
	      bufferT += 4;
	    }
	  else
	    {
	      esize = getUShort (bufferT, actualdicomimage->bigEndian);
	      bufferT++;
	      bufferT++;
	    }
	}
      else
	{
	  esize = getUInt (bufferT, actualdicomimage->bigEndian);
	  bufferT += 4;
	}
      /* We parse the code */
#ifdef _DICOM_PARANOID_DEBUG
      printf ("New code (%x,%x) at %d sized %d\n", group, element,
	      bufferT - buffer - 6, esize);
      fflush (NULL);
#endif
      dicomFieldIndex = searchDicomField (group, element);
      if (dicomFieldIndex == _MAX_DICOMFIELDS)
	{
	  unknownField = 1;
	  printf
	    ("WARNING: New code (%x,%x) at %d sized %d not found. Please update DICOM codes database.\n",
	     group, element, bufferT - buffer - 6, esize);
	  fflush (NULL);
	}
      else
	{
	  vr[0] = DICOMfields[dicomFieldIndex].vr[0];
	  vr[1] = DICOMfields[dicomFieldIndex].vr[1];
	  vr[2] = 0;
#ifdef _DICOM_PARANOID_DEBUG
	  printf ("%s,%s\n", DICOMfields[dicomFieldIndex].vr,
		  DICOMfields[dicomFieldIndex].longdescription);
#endif
	}

      if ((isSQ (vr)) && (esize == 0xffffffff))
	{			/* VR=SQ of undefined length */
	  esize = getSQsize (bufferT);
	}

      if ((group == 0x7FE0) && (element == 0x0010)
	  && (actualdicomimage->jpeg))
	{
	  char *jpegLocation;
	  unsigned int jpegSize;
	  getJPEGdata (bufferT, &jpegLocation, &jpegSize);
	  actualdicomimage->jpegImageLocation = jpegLocation;
	  actualdicomimage->jpegImageSize = jpegSize;
	  esize = jpegSize;
	}

      if ((group == 0x7FE0) && (element == 0x0010) && (actualdicomimage->rle))
	{
	  char *rleLocation;
	  unsigned int rleSize;
	  getRLEdata (bufferT, &rleLocation, &rleSize);
	  actualdicomimage->rleImageLocation = rleLocation;
	  actualdicomimage->rleImageSize = rleSize;
	  esize = rleSize;
	}

      /* Now, we read the text -if there is any- */
      fflush (NULL);
      if (esize != 0)
	{
#ifdef _DICOM_PARANOID_DEBUG
	  printf ("Making the insertion. The item size is %d\n", esize);
	  fflush (NULL);
#endif
	  value = malloc (sizeof (char) * (esize + 1));
	  if (value == NULL)
	    {
	      printf
		("ERROR: Could not allocate enough memory (%d) for the structure.",
		 esize);
	      fflush (NULL);
	      return (NULL);
	    }
	  memcpy (value, bufferT, esize);

	  if (!unknownField)
	    {
	      tempdicomhead =
		createDicomHeadItem (group, element, esize, dicomFieldIndex,
				     value, 0);
	    }
	  else
	    {
	      tempdicomhead =
		createDicomHeadItem (group, element, esize, dicomFieldIndex,
				     value, 1);
	    }

	  actualdicomhead =
	    insertDicomHeadItem (tempdicomhead, actualdicomhead);
	  bufferT += esize;

	  unknownField = 0;

	  /* Some special cases of parameters that are needed for the structure */
	  if (group == 0x0028)
	    {
	      switch (element)
		{
		  /* Image rows */
		case 0x0010:
		  {
		    actualdicomimage->height =
		      (unsigned long) getUShort (tempdicomhead->value,
						 actualdicomimage->bigEndian);
		    printf ("Image height (%d): %lu.\n",
			    tempdicomhead->valuesize,
			    actualdicomimage->height);
		    break;
		  }
		  /* Image columns */
		case 0x0011:
		  {
		    actualdicomimage->width =
		      (unsigned long) getUShort (tempdicomhead->value,
						 actualdicomimage->bigEndian);
		    printf ("Image width (%d): %lu.\n",
			    tempdicomhead->valuesize,
			    actualdicomimage->width);
		    break;
		  }
		  /* Image columns */
		case 0x100:
		  {
		    actualdicomimage->bitsAllocated =
		      (unsigned long) getUShort (tempdicomhead->value,
						 actualdicomimage->bigEndian);
		    printf ("Bits allocated: %i\n",
			    actualdicomimage->bitsAllocated);
		    break;
		  }
		case 0x102:
		  {
		    printf ("High bit: %i\n",
			    getUShort (tempdicomhead->value,
				       actualdicomimage->bigEndian));
		    break;
		  }
		case 0x103:
		  {
		    printf ("Pixel Representation: %i\n",
			    getUShort (tempdicomhead->value,
				       actualdicomimage->bigEndian));
		    actualdicomimage->withSign =
		      getUShort (tempdicomhead->value,
				 actualdicomimage->bigEndian);
		    break;
		  }
		case 0x0004:
		  {
		    printf ("Photometric: %s\n", tempdicomhead->value);
		    if (strcmp (tempdicomhead->value, "MONOCHROME1 ") == 0)
		      {
			actualdicomimage->grayScale = 1;
			actualdicomimage->monochromeMode = 1;
		      }
		    else if (strcmp (tempdicomhead->value, "MONOCHROME2 ") ==
			     0)
		      {
			actualdicomimage->grayScale = 1;
			actualdicomimage->monochromeMode = 2;
		      }
		    else if (strcmp (tempdicomhead->value, "RGB ") == 0)
		      {
			actualdicomimage->rgb = 1;
		      }
		    else if (strcmp (tempdicomhead->value, "PALETTE COLOR ")
			     == 0)
		      {
			actualdicomimage->paletteColor = 1;
		      }
		    else if (strcmp (tempdicomhead->value, "YBR_FULL") == 0)
		      {
			actualdicomimage->ybrFull = 1;
		      }
		    break;
		  }
		case 0x1101:
		  {		/*we look only for the first Palette Color Lookup Table Descriptor (Red), since the standard specifies that the values
				   of all the Table Descriptors shall be identical (pages 282 and 283 of the document 3 of the standard) */
		    if (actualdicomimage->paletteColor)
		      {
			char *tmp = tempdicomhead->value;
			unsigned short entriesNumberAux =
			  getUShort (tempdicomhead->value,
				     actualdicomimage->bigEndian);
			if (entriesNumberAux == 0)
			  {
			    actualdicomimage->paletteColorData.entriesNumber = 65536;	/* see standard */
			  }
			else
			  {
			    actualdicomimage->paletteColorData.entriesNumber =
			      entriesNumberAux;
			  }
			tempdicomhead->value += 2;
			actualdicomimage->paletteColorData.firstPixelValue =
			  getUShort (tempdicomhead->value,
				     actualdicomimage->bigEndian);
			tempdicomhead->value += 2;
			actualdicomimage->paletteColorData.bitsPerEntry =
			  getUShort (tempdicomhead->value,
				     actualdicomimage->bigEndian);
			tempdicomhead->value = tmp;
			break;
		      }
		  }
		case 0x1201:
		  {
		    actualdicomimage->paletteColorData.redTable =
		      actualdicomhead->value;
		    break;
		  }
		case 0x1202:
		  {
		    actualdicomimage->paletteColorData.greenTable =
		      actualdicomhead->value;
		    break;
		  }
		case 0x1203:
		  {
		    actualdicomimage->paletteColorData.blueTable =
		      actualdicomhead->value;
		    break;
		  }
		}
	    }

	  /* Finnaly, the image */
	  if ((group == 0x7fe0) && (element == 0x0010))
	    {
	      /* Here is the size */
	      actualdicomimage->size = tempdicomhead->valuesize;
	      printf ("Image sized (%d).\n", tempdicomhead->valuesize);
	      /* HACK: limited to one-image per file.
	       * The standar defines _animations_ in a file,
	       * but I have never seen a dancing x-ray image.
	       * If you want animations, you are in the wrong
	       * program. Use Osirix. */
	      actualdicomimage->image = tempdicomhead->value;
	    }

	}

#ifdef _DICOM_PARANOID_DEBUG
      fflush (NULL);
      printf ("Value:(%s),", tempdicomhead->value);
      fflush (NULL);
#endif

    }
#ifdef _DICOM_PARANOID_DEBUG
  printf ("\n Parsing ended.\n");
#endif
  actualdicomimage->head = actualdicomhead;

  if ((actualdicomimage->bitsAllocated == 16)
      && (actualdicomimage->grayScale))
    {				/* Find max and min pixel values, necessary to scale image to 8 bits */
      unsigned short int max, min;
      maxMinPix (actualdicomimage, &max, &min);
      actualdicomimage->maxPix = max;
      actualdicomimage->minPix = min;
    }
  else if ((actualdicomimage->paletteColorData.bitsPerEntry == 16)
	   && (actualdicomimage->paletteColor))
    {
      unsigned short int max, min;
      maxMinPixPaletteColor (actualdicomimage, &max, &min);
      actualdicomimage->paletteColorData.maxPix = max;
      actualdicomimage->paletteColorData.minPix = min;
    }

#ifdef _DICOM_PARANOID_DEBUG
  printHead (actualdicomhead);
#endif

  free (vr);
  free (transferSyntax);

  return actualdicomimage;
}
