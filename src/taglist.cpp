/***************************************************************************
 *   Copyright (C) 2008 by David Santo Orcero                              *
 *   irbis@orcero.org                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "taglist.h"

Taglist::Taglist (QWidget * parent, const char *name):
TaglistBase (parent, name)
{
}

/*
Taglist::Taglist(DICOMimage *fulltaglist,QWidget *parent, const char *name)
 : TaglistBase(parent, name)
{
*/
//m_taglisttable=new QTable(12,5);
//      m_taglisttable->setText(1,1,"Hola");
//m_taglisttable->setNumRows(row+1);
/*
	for ( int row = 0; row < 12; row++ ) {
for ( int col = 0; col < 5; col++ ) {
m_taglisttable->setText(row,col, QString::number( row * col ));
	//m_taglisttable->setItem( row, col,new QTableItem( m_taglisttable, QTableItem::WhenCurrent, QString::number( row * col ) ) );
}
}
  */
/*
}
*/

Taglist::~Taglist ()
{
}


bool
drawable (DICOMheadItem * item)
{
  if ((item->vr[0] == 'x') && (item->vr[1] == 's'))
    return false;
  if ((item->longdescription[0] == '?') && (item->longdescription[1] == 0))
    return false;
  return true;
};

void
Taglist::calculate (DICOMimage * dicomimage)
{
  DICOMheadItem *sig;
  int count = 0;
  int i;
  sig = dicomimage->head;
  while (NULL != sig)
    {
      if (drawable (sig))
	{
	  printf ("K");
	  fflush (NULL);
	  count++;
	}
      else
	{
	  printf ("(%d,%d)", sig->group, sig->element);
	  fflush (NULL);
	};
      sig = sig->next;
    };
  m_taglisttable->setNumRows (count);
  sig = dicomimage->head;
  printf ("\n------------------------------------\n");
  for (i = count - 1; i >= 0; i--)
    {
      if (drawable (sig))
	{
	  m_taglisttable->setText (i, 0, QString::number (sig->group));
	  m_taglisttable->setText (i, 1, QString::number (sig->element));
	  m_taglisttable->setText (i, 2, sig->vr);
	  m_taglisttable->setText (i, 3, sig->longdescription);
	  if (sig->vr[0] == 'U')
	    {
	      switch (sig->vr[1])
		{
		case 'L':
		  m_taglisttable->setText (i, 4,
					   QString::
					   number ((unsigned long int) sig->
						   value));
		  break;
		case 'S':
		  //m_taglisttable->setText(i,4,QString::number((unsigned short int)sig->value));
		  break;

		};
	    }
	  else
	    {
	      m_taglisttable->setText (i, 4, sig->value);
	    };
	}
      else
	{
	  printf ("(%d,%d)", sig->group, sig->element);
	  fflush (NULL);
	  i++;
	};
      sig = sig->next;

    };
}

#include "taglist.moc"
