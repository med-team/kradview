/***************************************************************************
 *   Copyright (C) 2008 by David Santo Orcero                              *
 *   irbis@orcero.org                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "kradview.h"
#include "gpl3.h"
#include <kapplication.h>
#include <dcopclient.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>

static const char description[] =
I18N_NOOP ("A fast and convenient DICOM viewer");

static const char version[] = "1.0";

static KCmdLineOptions options[] = {
  {"+[URL]", I18N_NOOP ("Document to open."), 0},
  KCmdLineLastOption
};

int
main (int argc, char **argv)
{
  KAboutData about ("kradview", I18N_NOOP ("kradview"), version, description,
		    KAboutData::License_GPL,
		    "(C) 2008 David Santo Orcero, under GPLv3", 0, 0,
		    "irbis@orcero.org");
  about.addAuthor ("David Santo Orcero", 0, "irbis@orcero.org");
  about.addAuthor ("David del Rio Medina", 0, "ddrm@alu.uma.es");
  about.setLicense (KAboutData::License_Custom);
  about.setLicenseText (I18N_NOOP (gpl3));
  KCmdLineArgs::init (argc, argv, &about);
  KCmdLineArgs::addCmdLineOptions (options);
  KApplication app;

  // register ourselves as a dcop client
  app.dcopClient ()->registerAs (app.name (), false);

  // see if we are starting with session management
  if (app.isRestored ())
    {
      RESTORE (kradview);
    }
  else
    {
      // no session.. just start up normally
      KCmdLineArgs *args = KCmdLineArgs::parsedArgs ();
      if (args->count () == 0)
	{
	  kradview *widget = new kradview;
	  widget->show ();
	}
      else
	{
	  int i = 0;
	  for (; i < args->count (); i++)
	    {
	      kradview *widget = new kradview;
	      widget->show ();
	      widget->load (args->url (i));
	    }
	}
      args->clear ();
    }

  return app.exec ();
}
