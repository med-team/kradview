/***************************************************************************
 *   Copyright (C) 2008 by David del Rio Medina, David Santo Orcero        *
 *   ddrm@alu.uma.es, irbis@orcero.org                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include <fcntl.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include <errno.h>

#include "structDicomHead.h"
#include "headDataBase.h"

void
printHead (DICOMhead dcmHead)
{
  DICOMhead dcmHPointer = dcmHead;
  while (dcmHPointer != NULL)
    {
      printf ("%x ", dcmHPointer->group);
      printf ("%x ", dcmHPointer->element);
      printf ("%s ", dcmHPointer->vr);
      printf ("%d ", dcmHPointer->valuesize);
      printf ("%s\n", dcmHPointer->longdescription);
      dcmHPointer = dcmHPointer->next;
    }
}

int
searchDicomField (unsigned short group, unsigned short element)
{
  unsigned int i;
  for (i = 0; i < _MAX_DICOMFIELDS; i++)
    {
      if ((group == DICOMfields[i].group)
	  && (element == DICOMfields[i].element))
	break;
    }
  return i;
}

DICOMhead
createDicomHeadItem (unsigned short group, unsigned short element,
		     unsigned int size, int dicomFieldIndex, char *value,
		     int unknownField)
{

  DICOMhead tempdicomhead;

  tempdicomhead = malloc (sizeof (DICOMheadItem));
  if (tempdicomhead == NULL)
    {
      printf ("ERROR: Could not allocate enough memory for the structure.");
      fflush (NULL);
      return (NULL);
    }
  tempdicomhead->group = group;
  tempdicomhead->element = element;
  tempdicomhead->valuesize = size;
  tempdicomhead->next = NULL;
  if (!unknownField)
    {
      tempdicomhead->vr[0] = DICOMfields[dicomFieldIndex].vr[0];
      tempdicomhead->vr[1] = DICOMfields[dicomFieldIndex].vr[1];
      tempdicomhead->vr[2] = 0;
      int descrsize = strlen (DICOMfields[dicomFieldIndex].longdescription);

      tempdicomhead->longdescription =
	malloc (sizeof (char) * (descrsize + 1));

      if (tempdicomhead->longdescription == NULL)
	{
	  printf
	    ("ERROR: Could not allocate enough memory for the structure.");
	  fflush (NULL);
	  return (NULL);
	};
      strncpy (tempdicomhead->longdescription,
	       DICOMfields[dicomFieldIndex].longdescription, descrsize);
      tempdicomhead->longdescription[descrsize] = 0;
    }
  else
    {
      tempdicomhead->longdescription = malloc (sizeof (char) * (14));
      strncpy (tempdicomhead->longdescription, "UNKNOWN FIELD", 13);
      tempdicomhead->longdescription[13] = 0;
    }

  tempdicomhead->value = value;
  tempdicomhead->value[size] = 0;

  return tempdicomhead;
}

DICOMhead
insertDicomHeadItem (DICOMhead item, DICOMhead list)
{				/*insert item at the beginning of list */
  if (list == NULL)
    {
      return item;
    }
  else
    {
      item->next = list;
      return item;
    }
}

void
freeDicomHead (DICOMhead list)
{
  DICOMhead aux;

  while (list != NULL)
    {
      aux = list;
      list = list->next;
      free (aux->longdescription);
      free (aux->value);
      free (aux);
    }
}
