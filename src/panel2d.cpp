/***************************************************************************
 *   Copyright (C) 2008 by David Santo Orcero, David del Rio Medina        *
 *   irbis@orcero.org, ddrm@alu.uma.es                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#define QT_CLEAN_NAMESPACE

#include "panel2d.h"
#include "dicomfformat.h"

#include <math.h>
#include <qlabel.h>
#include <qpixmap.h>
#include <qslider.h>
#include <qcanvas.h>
#include <qimage.h>
#include <kimageeffect.h>
#include <kcolorbutton.h>
#include <ktempfile.h>
#include <qcstring.h>
#include <cstdlib>

bool freeImage = false;

Panel2D::Panel2D (QWidget * parent, const char *name):Panel2DBase (parent,
	     name)
{
  m_zoomfactor = 1.5;
  m_canvas = new QCanvas (this, "Canvas");
  m_zoom = 1;
  m_dicomimage = NULL;
}


Panel2D::~Panel2D ()
{
}


void
Panel2D::updatePixmap ()
{
  QString buftempname;
  KTempFile buftemp;
// ASSERT: m_pixmap can't be null.
// It begins defined.



  m_canvas->setBackgroundColor (Qt::black);
  m_canvas->setBackgroundPixmap (m_pixmap);


  m_canvas->resize (m_pixmap.width (), m_pixmap.height ());

  m_dicomimageview->setCanvas (m_canvas);
//      m_dicomimageview->show();


}

void
Panel2D::draw16BitsGray ()
{
  uchar *paux;
  unsigned int x, y;
  int i;
  unsigned short actval;
  double scale = 1;
  double to8bits;

  m_pixmap =
    QImage::QImage (m_dicomimage->width, m_dicomimage->height, 8, 256);

  paux = (uchar *) m_dicomimage->image;
  if (m_dicomimage->maxPix != m_dicomimage->minPix)
    scale = 255. / (m_dicomimage->maxPix - m_dicomimage->minPix);
  for (i = 0; i < 256; i++)	// build color table
    m_pixmap.setColor (i, qRgb (i, i, i));	// (r,g,b) triplet is in grayscale when r == g == b

  for (y = 0; y < m_dicomimage->height; y++)
    {				// set image pixels
      uchar *p = m_pixmap.scanLine (y);
      for (x = 0; x < m_dicomimage->width; x++)
	{
	  actval = getUShort ((char *) paux, m_dicomimage->bigEndian);

	  if ((actval >= 32768) && (m_dicomimage->withSign))	//negative pixels not allowed, in the future an user option
	    actval = 0;
	  to8bits = scale * (actval - m_dicomimage->minPix);
	  actval = (unsigned short) nearbyint (to8bits);

	  if (actval > 255)
	    printf ("Pixel out of range:(%d)\n", actval);
	  paux++;
	  paux++;
	  if (m_dicomimage->monochromeMode == 1)
	    actval ^= 0x00ff;	//invertir colores
	  *p = actval;
	  p++;
	}
    }
  updatePixmap ();
}

void
Panel2D::draw8BitsGray ()
{
  char *paux;
  unsigned int x, y;
  int i;
  char actval;

  m_pixmap =
    QImage::QImage (m_dicomimage->width, m_dicomimage->height, 8, 256);

  paux = (char *) m_dicomimage->image;
  for (i = 0; i < 256; i++)	// build color table
    m_pixmap.setColor (i, qRgb (i, i, i));	// (r,g,b) triplet is in grayscale when r == g == b

  for (y = 0; y < m_dicomimage->height; y++)
    {				// set image pixels
      char *p = (char *) m_pixmap.scanLine (y);
      for (x = 0; x < m_dicomimage->width; x++)
	{
	  actval = *paux;

	  paux++;
	  if (m_dicomimage->monochromeMode == 1)
	    actval ^= 0xff;	//invertir colores
	  *p = actval;
	  p++;
	}
    }
  updatePixmap ();
}

void
Panel2D::draw8BitsRGB ()
{
  char *paux;
  unsigned int x, y;
  char r, g, b;

  m_pixmap =
    QImage::QImage (m_dicomimage->width, m_dicomimage->height, 32, 16777216);

  paux = (char *) m_dicomimage->image;
  for (y = 0; y < m_dicomimage->height; y++)
    {				// set image pixels
      uint *p = (uint *) m_pixmap.scanLine (y);
      for (x = 0; x < m_dicomimage->width; x++)
	{
	  r = *paux;
	  paux++;
	  g = *paux;
	  paux++;
	  b = *paux;
	  paux++;
	  *p = qRgb (r, g, b);
	  p++;
	}
    }
  updatePixmap ();
}

void
Panel2D::draw16BitsPaletteColor8BitsAlloc ()
{
  char *paux, *mapped;
  unsigned char index;
  unsigned short offset, actval;
  unsigned int x, y;
  char r, g, b;
  double to8bits;
  double scale = 1;

  m_pixmap =
    QImage::QImage (m_dicomimage->width, m_dicomimage->height, 32, 16777216);

  paux = (char *) m_dicomimage->image;
  if (m_dicomimage->paletteColorData.maxPix !=
      m_dicomimage->paletteColorData.minPix)
    scale =
      255. / (m_dicomimage->paletteColorData.maxPix -
	      m_dicomimage->paletteColorData.minPix);

  for (y = 0; y < m_dicomimage->height; y++)
    {				// set image pixels
      uint *p = (uint *) m_pixmap.scanLine (y);
      for (x = 0; x < m_dicomimage->width; x++)
	{
	  index = *paux;
	  if (index <= m_dicomimage->paletteColorData.firstPixelValue)
	    {
	      offset = 0;
	    }
	  else if (index >=
		   (m_dicomimage->paletteColorData.entriesNumber +
		    m_dicomimage->paletteColorData.firstPixelValue - 1))
	    {
	      offset = (m_dicomimage->paletteColorData.entriesNumber - 1) * 2;
	    }
	  else
	    {
	      offset =
		(index - m_dicomimage->paletteColorData.firstPixelValue) * 2;
	    }
	  mapped = m_dicomimage->paletteColorData.redTable + offset;
	  actval = getUShort (mapped, m_dicomimage->bigEndian);
	  to8bits = scale * (actval - m_dicomimage->paletteColorData.minPix);
	  actval = (unsigned short) nearbyint (to8bits);
	  r = (char) actval;
	  mapped = m_dicomimage->paletteColorData.greenTable + offset;
	  actval = getUShort (mapped, m_dicomimage->bigEndian);
	  to8bits = scale * (actval - m_dicomimage->paletteColorData.minPix);
	  actval = (unsigned short) nearbyint (to8bits);
	  g = (char) actval;
	  mapped = m_dicomimage->paletteColorData.blueTable + offset;
	  actval = getUShort (mapped, m_dicomimage->bigEndian);
	  to8bits = scale * (actval - m_dicomimage->paletteColorData.minPix);
	  actval = (unsigned short) nearbyint (to8bits);
	  b = (char) actval;
	  *p = qRgb (r, g, b);
	  p++;
	  paux++;
	}
    }
  updatePixmap ();
}

void
Panel2D::draw8BitsPaletteColor8BitsAlloc ()
{
  char *paux, *mapped;
  unsigned char index;
  unsigned short offset;
  unsigned int x, y;
  char r, g, b;

  m_pixmap =
    QImage::QImage (m_dicomimage->width, m_dicomimage->height, 32, 16777216);

  paux = (char *) m_dicomimage->image;
  for (y = 0; y < m_dicomimage->height; y++)
    {				// set image pixels
      uint *p = (uint *) m_pixmap.scanLine (y);
      for (x = 0; x < m_dicomimage->width; x++)
	{
	  index = *paux;
	  if (index <= m_dicomimage->paletteColorData.firstPixelValue)
	    {
	      offset = 0;
	    }
	  else if (index >=
		   (m_dicomimage->paletteColorData.entriesNumber +
		    m_dicomimage->paletteColorData.firstPixelValue - 1))
	    {
	      offset = (m_dicomimage->paletteColorData.entriesNumber - 1);
	    }
	  else
	    {
	      offset =
		(index - m_dicomimage->paletteColorData.firstPixelValue);
	    }

	  /* even if palette color table data is 8 bits per entry, date is stored in words (so, it changes in big endian)
	     this is a way to get the offset pointed to the correct byte, another way could be convert the tables to little endian */
	  if (m_dicomimage->bigEndian)
	    {
	      if ((offset % 2) == 0)
		{
		  offset++;
		}
	      else
		{
		  offset--;
		}
	    }

	  mapped = m_dicomimage->paletteColorData.redTable + offset;
	  r = *mapped;
	  mapped = m_dicomimage->paletteColorData.greenTable + offset;
	  g = *mapped;
	  mapped = m_dicomimage->paletteColorData.blueTable + offset;
	  b = *mapped;
	  *p = qRgb (r, g, b);
	  p++;
	  paux++;
	}
    }
  updatePixmap ();
}

void
Panel2D::draw8BitsPaletteColor16BitsAlloc ()
{
  char *mapped;
  uchar *paux;
  unsigned char index;
  unsigned short offset;
  unsigned int x, y;
  char r, g, b;


  m_pixmap =
    QImage::QImage (m_dicomimage->width, m_dicomimage->height, 32, 16777216);

  paux = (uchar *) m_dicomimage->image;
  for (y = 0; y < m_dicomimage->height; y++)
    {				// set image pixels
      uint *p = (uint *) m_pixmap.scanLine (y);
      for (x = 0; x < m_dicomimage->width; x++)
	{
	  index = getUShort ((char *) paux, m_dicomimage->bigEndian);
	  if (index <= m_dicomimage->paletteColorData.firstPixelValue)
	    {
	      offset = 0;
	    }
	  else if (index >=
		   (m_dicomimage->paletteColorData.entriesNumber +
		    m_dicomimage->paletteColorData.firstPixelValue - 1))
	    {
	      offset = (m_dicomimage->paletteColorData.entriesNumber - 1);
	    }
	  else
	    {
	      offset =
		(index - m_dicomimage->paletteColorData.firstPixelValue);
	    }

	  /* even if palette color table data is 8 bits per entry, date is stored in words (so, it changes in big endian)
	     this is a way to get the offset pointed to the correct byte, another way could be convert the tables to little endian */
	  if (m_dicomimage->bigEndian)
	    {
	      if ((offset % 2) == 0)
		{
		  offset++;
		}
	      else
		{
		  offset--;
		}
	    }

	  mapped = m_dicomimage->paletteColorData.redTable + offset;
	  r = *mapped;
	  mapped = m_dicomimage->paletteColorData.greenTable + offset;
	  g = *mapped;
	  mapped = m_dicomimage->paletteColorData.blueTable + offset;
	  b = *mapped;
	  *p = qRgb (r, g, b);
	  p++;
	  paux += 2;
	}
    }
  updatePixmap ();
}

void
Panel2D::draw16BitsPaletteColor16BitsAlloc ()
{
  char *mapped;
  uchar *paux;
  unsigned char index;
  unsigned short offset, actval;
  unsigned int x, y;
  char r, g, b;
  double to8bits;
  double scale = 1;

  m_pixmap =
    QImage::QImage (m_dicomimage->width, m_dicomimage->height, 32, 16777216);

  paux = (uchar *) m_dicomimage->image;
  if (m_dicomimage->paletteColorData.maxPix !=
      m_dicomimage->paletteColorData.minPix)
    scale =
      255. / (m_dicomimage->paletteColorData.maxPix -
	      m_dicomimage->paletteColorData.minPix);

  for (y = 0; y < m_dicomimage->height; y++)
    {				// set image pixels
      uint *p = (uint *) m_pixmap.scanLine (y);
      for (x = 0; x < m_dicomimage->width; x++)
	{
	  index = getUShort ((char *) paux, m_dicomimage->bigEndian);
	  if (index <= m_dicomimage->paletteColorData.firstPixelValue)
	    {
	      offset = 0;
	    }
	  else if (index >=
		   (m_dicomimage->paletteColorData.entriesNumber +
		    m_dicomimage->paletteColorData.firstPixelValue - 1))
	    {
	      offset = (m_dicomimage->paletteColorData.entriesNumber - 1) * 2;
	    }
	  else
	    {
	      offset =
		(index - m_dicomimage->paletteColorData.firstPixelValue) * 2;
	    }
	  mapped = m_dicomimage->paletteColorData.redTable + offset;
	  actval = getUShort (mapped, m_dicomimage->bigEndian);
	  to8bits = scale * (actval - m_dicomimage->paletteColorData.minPix);
	  actval = (unsigned short) nearbyint (to8bits);
	  r = (char) actval;
	  mapped = m_dicomimage->paletteColorData.greenTable + offset;
	  actval = getUShort (mapped, m_dicomimage->bigEndian);
	  to8bits = scale * (actval - m_dicomimage->paletteColorData.minPix);
	  actval = (unsigned short) nearbyint (to8bits);
	  g = (char) actval;
	  mapped = m_dicomimage->paletteColorData.blueTable + offset;
	  actval = getUShort (mapped, m_dicomimage->bigEndian);
	  to8bits = scale * (actval - m_dicomimage->paletteColorData.minPix);
	  actval = (unsigned short) nearbyint (to8bits);
	  b = (char) actval;
	  *p = qRgb (r, g, b);
	  p++;
	  paux += 2;
	}
    }
  updatePixmap ();
}

void
Panel2D::draw8BitsYBRFull ()
{
  char *paux;
  unsigned int x, y;
  unsigned char r, g, b, yComp, cb, cr;

  m_pixmap =
    QImage::QImage (m_dicomimage->width, m_dicomimage->height, 32, 16777216);

  paux = (char *) m_dicomimage->image;
  for (y = 0; y < m_dicomimage->height; y++)
    {				// set image pixels
      uint *p = (uint *) m_pixmap.scanLine (y);
      for (x = 0; x < m_dicomimage->width; x++)
	{
	  yComp = *paux;
	  paux++;
	  cb = *paux;
	  paux++;
	  cr = *paux;
	  paux++;
	  YBRFullToRGB (yComp, cb, cr, &r, &g, &b);
	  *p = qRgb (r, g, b);
	  p++;
	}
    }
  updatePixmap ();
}

void
Panel2D::YBRFullToRGB (unsigned char Y, unsigned char Cb, unsigned char Cr,
		       unsigned char *r, unsigned char *g, unsigned char *b)
{
  double rAux, gAux, bAux;

  rAux = Y + (1.402 * (Cr - 128));
  if (rAux < 0)
    rAux = 0;			/* correct some precision errors that give near-zero negative numbers */
  gAux = Y - (0.34414 * (Cb - 128)) - (0.71414 * (Cr - 128));
  if (gAux < 0)
    gAux = 0;
  bAux = Y + (1.772 * (Cb - 128));
  if (bAux < 0)
    bAux = 0;

  *r = (unsigned char) nearbyint (rAux);
  *g = (unsigned char) nearbyint (gAux);
  *b = (unsigned char) nearbyint (bAux);
}

void
decompRLEsegment (char *segPointer, char *segData,
		  unsigned int decompSegmentSize)
{
/*	Loop until the number of output bytes equals the uncompressed segment size
		Read the next source byte into n
		If n> =0 and n <= 127 then
			output the next n+1 bytes literally
		Elseif n <= - 1 and n >= -127 then
			output the next byte -n+1 times
		Elseif n = - 128 then
			output nothing
		Endif
	Endloop */

  char n;
  unsigned char i;
  unsigned int counter = 0;

  n = *segPointer;
  segPointer++;
  while (counter < decompSegmentSize)
    {
      if ((n >= 0) && (n <= 127))
	{
	  for (i = 0; i <= n; i++)
	    {
	      *segData = *segPointer;
	      segData++;
	      segPointer++;
	    }
	  counter += (n + 1);
	}
      else if ((n <= -1) && (n >= -127))
	{
	  for (i = 0; i <= (abs (n)); i++)
	    {
	      *segData = *segPointer;
	      segData++;
	    }
	  segPointer++;
	  counter += ((-n) + 1);
	}
      n = *segPointer;
      segPointer++;
    }
}

void
Panel2D::drawRLE ()
{
  char *pointer;
  unsigned int segmentsNumber;
  char *segmentPointers[15];	//maximum number of segments
  char *segmentData[15];
  unsigned int i, j;
  char *rawImage;

  pointer = m_dicomimage->rleImageLocation;
  segmentsNumber = *(unsigned int *) pointer;

  pointer += 4;			// now we are in the offset of the first segment
  for (i = 0; i < segmentsNumber; i++)
    {
      segmentPointers[i] =
	(*(unsigned int *) pointer) + (m_dicomimage->rleImageLocation);
      pointer += 4;
    }

  unsigned int decompSegmentSize =
    m_dicomimage->height * m_dicomimage->width *
    (m_dicomimage->bitsAllocated / 8);
  for (i = 0; i < segmentsNumber; i++)
    {
      segmentData[i] = (char *) malloc (decompSegmentSize);
      decompRLEsegment (segmentPointers[i], segmentData[i],
			decompSegmentSize);
    }

  rawImage = (char *) malloc (decompSegmentSize * segmentsNumber);
  char *dataPointer;
  for (i = 0; i < decompSegmentSize; i++)
    {
      for (j = 0; j < segmentsNumber; j++)
	{
	  dataPointer = segmentData[j] + i;
	  rawImage[(i * segmentsNumber) + j] = *dataPointer;
	}
    }

  m_dicomimage->image = rawImage;
  for (i = 0; i < segmentsNumber; i++)
    {
      free (segmentData[i]);
    }
  rawImage = NULL;

  if (m_dicomimage->grayScale)
    {
      if (m_dicomimage->bitsAllocated == 16)
	{
	  draw16BitsGray ();
	}
      else
	{
	  draw8BitsGray ();
	}
    }
  else if (m_dicomimage->rgb)
    {
      draw8BitsRGB ();
    }
  else if (m_dicomimage->paletteColor)
    {
      if (m_dicomimage->paletteColorData.bitsPerEntry == 16)
	{
	  if (m_dicomimage->bitsAllocated == 8)
	    {
	      draw16BitsPaletteColor8BitsAlloc ();
	    }
	  else
	    {
	      draw16BitsPaletteColor16BitsAlloc ();
	    }
	}
      else
	{
	  if (m_dicomimage->bitsAllocated == 8)
	    {
	      draw8BitsPaletteColor8BitsAlloc ();
	    }
	  else
	    {
	      draw8BitsPaletteColor16BitsAlloc ();
	    }
	}
    }
  else if (m_dicomimage->ybrFull)
    {
      draw8BitsYBRFull ();
    }

  free (m_dicomimage->image);
}

void
Panel2D::setDICOM (DICOMimage * dicomimage)
{
  if (freeImage)
    {
      freeDicomImage (m_dicomimage);
    }
  else
    {
      freeImage = true;
    }

  m_dicomimage = dicomimage;
  dicomimage = NULL;

  if (m_dicomimage->jpeg)
    {				/* encapsulated formats (JPEG, RLE, etc) have priority, because the image can also be rgb, grayScale, etc. */
      m_pixmap.loadFromData ((uchar *) m_dicomimage->jpegImageLocation,
			     m_dicomimage->jpegImageSize, "JPEG");
      updatePixmap ();
    }
  else if (m_dicomimage->rle)
    {
      drawRLE ();
    }
  else if (m_dicomimage->grayScale)
    {
      if (m_dicomimage->bitsAllocated == 16)
	{
	  draw16BitsGray ();
	}
      else
	{
	  draw8BitsGray ();
	}
    }
  else if (m_dicomimage->rgb)
    {
      draw8BitsRGB ();
    }
  else if (m_dicomimage->paletteColor)
    {
      if (m_dicomimage->paletteColorData.bitsPerEntry == 16)
	{
	  if (m_dicomimage->bitsAllocated == 8)
	    {
	      draw16BitsPaletteColor8BitsAlloc ();
	    }
	  else
	    {
	      draw16BitsPaletteColor16BitsAlloc ();
	    }
	}
      else
	{
	  if (m_dicomimage->bitsAllocated == 8)
	    {
	      draw8BitsPaletteColor8BitsAlloc ();
	    }
	  else
	    {
	      draw8BitsPaletteColor16BitsAlloc ();
	    }
	}
    }
  else if (m_dicomimage->ybrFull)
    {
      draw8BitsYBRFull ();
    }

  m_pixmap.reset ();
}


void
Panel2D::zoomin ()
{
  QWMatrix wm;
  m_zoom *= m_zoomfactor;
  wm.scale (m_zoom, m_zoom);
  m_dicomimageview->setWorldMatrix (wm);
}

void
Panel2D::zoomout ()
{
  QWMatrix wm;
  m_zoom = m_zoom / m_zoomfactor;
  wm.scale (m_zoom, m_zoom);
  m_dicomimageview->setWorldMatrix (wm);
}

/*
void Panel2D::scale()
{


   int h = height(); //- statusBar->heightForWidth( width() ) - status->height();

   if ( m_pixmap.isNull() ) return;

   QApplication::setOverrideCursor( waitCursor ); // this might take time
   if ( width() == pm.width() && h == pm.height() )
   {                                           // no need to scale if widget
       pmScaled = pm;                          // size equals pixmap size
   } else {
       if (smooth()) {
           pmScaled.convertFromImage(m_pixmap.smoothScale(width(), h),
               conversion_flags);
       } else {
           QWMatrix m;                         // transformation matrix
           m.scale(((double)width())/pm.width(),// define scale factors
                   ((double)h)/pm.height());
           pmScaled = pm.xForm( m );           // create scaled pixmap
       }
   }
   QApplication::restoreOverrideCursor();      // restore original cursor
}
*/

/*
  The resize event handler, if a valid pixmap was loaded it will call
  scale() to fit the pixmap to the new widget size.
*/
/*
void Panel2D::resizeEvent( QResizeEvent * )
{
    status->setGeometry(0, height() - status->height(),
                        width(), status->height());

    if ( pm.size() == QSize( 0, 0 ) )           // we couldn't load the image
        return;

    int h = height() - menubar->heightForWidth( width() ) - status->height();
    if ( width() != pmScaled.width() || h != pmScaled.height())
    {                                           // if new size,
        scale();                                // scale pmScaled to window
        updateStatus();
    }
    if ( image.hasAlphaBuffer() )
        erase();
}

bool Panel2D::convertEvent( QMouseEvent* e, int& x, int& y)
{
    if ( pm.size() != QSize( 0, 0 ) ) {
        int h = height() - menubar->heightForWidth( width() ) - status->height();
        int nx = e->x() * image.width() / width();
        int ny = (e->y()-menubar->heightForWidth( width() )) * image.height() / h;
        if (nx != x || ny != y ) {
            x = nx;
            y = ny;
            updateStatus();
            return TRUE;
        }
    }
    return FALSE;
}

*/
#include "panel2d.moc"
