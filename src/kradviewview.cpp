/***************************************************************************
 *   Copyright (C) 2008 by David Santo Orcero                              *
 *   irbis@orcero.org                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "kradviewview.h"

#include <qpainter.h>
#include <qlayout.h>

#include <kurl.h>

#include <ktempfile.h>
#include <kio/netaccess.h>

#include <ktrader.h>
#include <klibloader.h>
#include <kmessagebox.h>
#include <krun.h>
#include <klocale.h>

#include "dicomfformat.h"

kradviewView::kradviewView (QWidget * parent):QWidget (parent),
DCOPObject ("kradviewIface")
{
  // setup our layout manager to automatically add our widgets
  QHBoxLayout *
    top_layout = new QHBoxLayout (this);
  top_layout->setAutoAdd (true);

  // create an empty DICOM 2D panel viewer at the beginning of the
  // application as default.
  m_Panel2D = new Panel2D (this, "Panel2D");

  /*
     connect(m_html, SIGNAL(setWindowCaption(const QString&)),
     this,   SLOT(slotSetTitle(const QString&)));
     connect(m_html, SIGNAL(setStatusBarText(const QString&)),
     this,   SLOT(slotOnURL(const QString&)));
   */
}

kradviewView::~kradviewView ()
{
}

void
kradviewView::print (QPainter * p, int height, int width)
{
  // do the actual printing, here
  // p->drawText(etc..)
}

QString
kradviewView::currentURL ()
{
  return m_html->url ().url ();
}



void
kradviewView::openURL (QString url)
{
  openURL (KURL (url));
}

void
kradviewView::openURL (const KURL & url)
{
  DICOMimage *loadedfile;
  QString tmpFile;
  if (KIO::NetAccess::download (url, tmpFile, this))
    {
// load in the file (target is always local)
      loadedfile = loadImage (tmpFile.ascii ());
      if (_ERROR_CANTOPEN == loadedfile)
	{
	  KMessageBox::sorry (0, "Error while opening the file.", "Error", 0);
	}
      else if (_ERROR_ISNOTDICOM == loadedfile)
	{
	  KMessageBox::sorry (0,
			      "This file does not seem to be a DICOM file.\nIf you think this is an error, contact the authors.",
			      "Error", 0);
	}
      else if (_ERROR_ISNOTSUPPORTED == loadedfile)
	{
	  KMessageBox::sorry (0,
			      "This format is NOT supported for the moment",
			      "Error", 0);
	}
      else
	{
	  m_Panel2D->setDICOM (loadedfile);
	};
    }
}


void
kradviewView::slotOnURL (const QString & url)
{
  emit signalChangeStatusbar (url);
}

void
kradviewView::slotSetTitle (const QString & title)
{
  emit signalChangeCaption (title);
}



void
kradviewView::saveURL (const KURL & url)
{
  QString tmpFile;
  KTempFile *temp = 0;
  if (url.isLocalFile ())
    tmpFile = url.path ();
  else
    {
      temp = new KTempFile;
      tmpFile = temp->name ();
    }
  if (temp)
    {
      KIO::NetAccess::upload (tmpFile, url, this);
      temp->unlink ();
      delete temp;
    }
}

#include "kradviewview.moc"
