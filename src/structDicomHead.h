/***************************************************************************
 *   Copyright (C) 2008 by David Santo Orcero                              *
 *   irbis@orcero.org                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


typedef struct structDICOMheadItem DICOMheadItem;

typedef DICOMheadItem *DICOMhead;

struct structDICOMheadItem
{
  unsigned short group;
  unsigned short element;
  char vr[3];
  char *longdescription;
  unsigned int valuesize;
  char *value;
  DICOMhead next;
};

unsigned int getSQsize (char *pointer);
char isJPEG (char *transferSyntax);
void getJPEGdata (char *image, char **jpegImageLocation,
		  unsigned int *jpegImageSize);
void printHead (DICOMhead dcmHead);
int searchDicomField (unsigned short group, unsigned short element);
DICOMhead createDicomHeadItem (unsigned short group, unsigned short element,
			       unsigned int size, int dicomFieldIndex,
			       char *value, int unknownField);
DICOMhead insertDicomHeadItem (DICOMhead item, DICOMhead list);	/*insert item at the beginning of list */
void freeDicomHead (DICOMhead list);
