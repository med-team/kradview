/***************************************************************************
 *   Copyright (C) 2008 by David Santo Orcero                              *
 *   irbis@orcero.org                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "kradview.h"
#include "pref.h"
#include "taglist.h"

#include <qdragobject.h>
#include <kprinter.h>
#include <qpainter.h>
#include <qpaintdevicemetrics.h>

#include <kglobal.h>
#include <klocale.h>
#include <kiconloader.h>
#include <kdeversion.h>
#include <kmenubar.h>
#include <kstatusbar.h>
#include <kpopupmenu.h>
#include <kkeydialog.h>
#include <kaccel.h>
#include <kio/netaccess.h>
#include <kfiledialog.h>
#include <kconfig.h>
#include <kurl.h>
#include <kurldrag.h>
#include <kurlrequesterdlg.h>

#include <kedittoolbar.h>

#include <kstdaccel.h>
#include <kaction.h>
#include <kstdaction.h>

// We include here the custom icons
// -most of them borrowed from kivio
/*
#include "zoom_in.xpm"
#include "zoom_out.xpm"
#include "zoom_selected.xpm"
*/
#include "adjustlevels.xpm"
#include "info.xpm"


kradview::kradview ():KMainWindow (0, "kradview"),
m_view (new kradviewView (this)), m_printer (0)
{
  // accept dnd
  setAcceptDrops (true);

  // tell the KMainWindow that this is indeed the main widget
  setCentralWidget (m_view);


  // then, setup our actions
  setupActions ();

  // and a status bar
  statusBar ()->show ();

  // apply the saved mainwindow settings, if any, and ask the mainwindow
  // to automatically save settings if changed: window size, toolbar
  // position, icon size, etc.
  setAutoSaveSettings ();

  // allow the view to change the statusbar and caption
  connect (m_view, SIGNAL (signalChangeStatusbar (const QString &)),
	   this, SLOT (changeStatusbar (const QString &)));
  connect (m_view, SIGNAL (signalChangeCaption (const QString &)),
	   this, SLOT (changeCaption (const QString &)));

}

kradview::~kradview ()
{
}

void
kradview::load (const KURL & url)
{
  QString target;
  // the below code is what you should normally do.  in this
  // example case, we want the url to our own.  you probably
  // want to use this code instead for your app

#if 0
  // download the contents
  if (KIO::NetAccess::download (url, target))
    {
      // set our caption
      setCaption (url);

      // load in the file (target is always local)
      loadFile (target);

      // and remove the temp file
      KIO::NetAccess::removeTempFile (target);
    }
#endif

  setCaption (url.prettyURL ());
  m_view->openURL (url);
}

void
kradview::setupActions ()
{

// We create the popup menus
//
  KPopupMenu *menuFile = new KPopupMenu (this);
  KPopupMenu *menuEdit = new KPopupMenu (this);

  menuBar ()->insertItem ("&File", menuFile);
  menuBar ()->insertItem ("&Edit", menuEdit);
  menuBar ()->insertItem ("&Help", helpMenu ());

// We create the standard actions
  KAction *actionNew = KStdAction::openNew (this, SLOT (fileNew ()), 0);
  KAction *actionOpen = KStdAction::open (this, SLOT (fileOpen ()), 0);
  KAction *actionSave = KStdAction::save (this, SLOT (fileSave ()), 0);
  KAction *actionQuit = KStdAction::quit (this, SLOT (close ()), 0);
  KAction *actionzoomin = KStdAction::zoomIn (this, SLOT (zoomin ()), 0);
  KAction *actionzoomout = KStdAction::zoomOut (this, SLOT (zoomout ()), 0);
  KAction *actionzoomselected = KStdAction::zoom (this, SLOT (nop ()), 0);

// Now we add the custom actions
// zoom in, zoom out, zoom select and info
  /*
     KAction *actionzoomin =
     new KAction( "Zoom &In", QIconSet(QPixmap(icon_zoom_in)),
     ALT+Key_I, this, SLOT(zoomin()), this, "I");
     KAction *actionzoomout =
     new KAction( "Zoom &Out", QIconSet(QPixmap(icon_zoom_out)),
     ALT+Key_O, this, SLOT(zoomout()), this, "O");
     KAction *actionzoomselected =
     new KAction( "Zoom &Select", QIconSet(QPixmap(icon_zoom_select)),
     ALT+Key_S, this, SLOT(nop()), this, "S");
   */
  KAction *actionadjustlevels =
    new KAction ("Adjust &Levels", QIconSet (QPixmap (icon_adjustlevels)),
		 ALT + Key_L, this, SLOT (nop ()), this, "S");
  KAction *actioninfo = new KAction ("Inf&o", QIconSet (QPixmap (icon_info)),
				     ALT + Key_O, this, SLOT (info ()), this,
				     "O");

  toolBar ()->setFullSize (true);
  actionNew->plug (menuFile);
  actionOpen->plug (menuFile);
  actionSave->plug (menuFile);
  menuFile->insertSeparator ();
  actionQuit->plug (menuFile);


  actionzoomin->plug (menuEdit);
  actionzoomout->plug (menuEdit);
  actionzoomselected->plug (menuEdit);
  menuEdit->insertSeparator ();
  actionadjustlevels->plug (menuEdit);
  menuEdit->insertSeparator ();
  actioninfo->plug (menuEdit);


  actionNew->plug (toolBar ());
  actionOpen->plug (toolBar ());
  actionSave->plug (toolBar ());
  toolBar ()->insertLineSeparator (3, 3);


  actionzoomin->plug (toolBar ());
  actionzoomout->plug (toolBar ());
  actionzoomselected->plug (toolBar ());
  toolBar ()->insertLineSeparator (7, 7);
  actionadjustlevels->plug (toolBar ());
  toolBar ()->insertLineSeparator (9, 9);
  actioninfo->plug (toolBar ());
  toolBar ()->insertLineSeparator (11, 11);
  int codquit = actionQuit->plug (toolBar ());
  toolBar ()->alignItemRight (codquit, true);


  /*
     KStdAction::openNew(this, SLOT(fileNew()), actionCollection());
     KStdAction::open(this, SLOT(fileOpen()), actionCollection());
     KStdAction::save(this, SLOT(fileSave()), actionCollection());
     KStdAction::saveAs(this, SLOT(fileSaveAs()), actionCollection());
     KStdAction::print(this, SLOT(filePrint()), actionCollection());
     KStdAction::quit(kapp, SLOT(quit()), actionCollection());




     m_toolbarAction = KStdAction::showToolbar(this, SLOT(optionsShowToolbar()), actionCollection());
     m_statusbarAction = KStdAction::showStatusbar(this, SLOT(optionsShowStatusbar()), actionCollection());

     KStdAction::keyBindings(this, SLOT(optionsConfigureKeys()), actionCollection());
     KStdAction::configureToolbars(this, SLOT(optionsConfigureToolbars()), actionCollection());
     KStdAction::preferences(this, SLOT(optionsPreferences()), actionCollection());


     createGUI();
   */
}

void
kradview::saveProperties (KConfig * config)
{
  // the 'config' object points to the session managed
  // config file.  anything you write here will be available
  // later when this app is restored

  if (!m_view->currentURL ().isEmpty ())
    {
#if KDE_IS_VERSION(3,1,3)
      config->writePathEntry ("lastURL", m_view->currentURL ());
#else
      config->writeEntry ("lastURL", m_view->currentURL ());
#endif
    }
}

void
kradview::readProperties (KConfig * config)
{
  // the 'config' object points to the session managed
  // config file.  this function is automatically called whenever
  // the app is being restored.  read in here whatever you wrote
  // in 'saveProperties'

  QString url = config->readPathEntry ("lastURL");

  if (!url.isEmpty ())
    m_view->openURL (KURL (url));
}

void
kradview::dragEnterEvent (QDragEnterEvent * event)
{
  // accept uri drops only
  event->accept (KURLDrag::canDecode (event));
}

void
kradview::dropEvent (QDropEvent * event)
{
  // this is a very simplistic implementation of a drop event.  we
  // will only accept a dropped URL.  the Qt dnd code can do *much*
  // much more, so please read the docs there
  KURL::List urls;

  // see if we can decode a URI.. if not, just ignore it
  if (KURLDrag::decode (event, urls) && !urls.isEmpty ())
    {
      // okay, we have a URI.. process it
      const KURL & url = urls.first ();

      // load in the file
      load (url);
    }
}

void
kradview::fileNew ()
{
  // this slot is called whenever the File->New menu is selected,
  // the New shortcut is pressed (usually CTRL+N) or the New toolbar
  // button is clicked

  // create a new window
  (new kradview)->show ();
}

void
kradview::fileOpen ()
{
  // this slot is called whenever the File->Open menu is selected,
  // the Open shortcut is pressed (usually CTRL+O) or the Open toolbar
  // button is clicked
  /*
     // this brings up the generic open dialog
     KURL url = KURLRequesterDlg::getURL(QString::null, this, i18n("Open Location") );
   */
  // standard filedialog
  KURL url =
    KFileDialog::getOpenURL (QString::null, QString::null, this,
			     i18n ("Open Location"));
  if (!url.isEmpty ())
    {
      // Here we open the DICOM file
      m_view->openURL (url);
    };
}

void
kradview::fileSave ()
{
  // this slot is called whenever the File->Save menu is selected,
  // the Save shortcut is pressed (usually CTRL+S) or the Save toolbar
  // button is clicked

  // save [NOT IMPLEMENTED]


}

void
kradview::fileSaveAs ()
{
  // this slot is called whenever the File->Save As menu is selected,
  //
  //KURL file_url = KFileDialog::getSaveURL();
  //if (!file_url.isEmpty() && !file_url.isMalformed())
  //{
  // save [NOT IMPLEMENTED]
  //}
}

void
kradview::filePrint ()
{
  // this slot is called whenever the File->Print menu is selected,
  // the Print shortcut is pressed (usually CTRL+P) or the Print toolbar
  // button is clicked
  if (!m_printer)
    m_printer = new KPrinter;
  if (m_printer->setup (this))
    {
      // setup the printer.  with Qt, you always "print" to a
      // QPainter.. whether the output medium is a pixmap, a screen,
      // or paper
      QPainter p;
      p.begin (m_printer);

      // we let our view do the actual printing
      QPaintDeviceMetrics metrics (m_printer);
      m_view->print (&p, metrics.height (), metrics.width ());

      // and send the result to the printer
      p.end ();
    }
}

void
kradview::optionsShowToolbar ()
{
  // this is all very cut and paste code for showing/hiding the
  // toolbar
  if (m_toolbarAction->isChecked ())
    toolBar ()->show ();
  else
    toolBar ()->hide ();
}

void
kradview::optionsShowStatusbar ()
{
  // this is all very cut and paste code for showing/hiding the
  // statusbar
  if (m_statusbarAction->isChecked ())
    statusBar ()->show ();
  else
    statusBar ()->hide ();
}

void
kradview::optionsConfigureKeys ()
{
  KKeyDialog::configureKeys (actionCollection (), "kradviewui.rc");
}

void
kradview::optionsConfigureToolbars ()
{
  // use the standard toolbar editor
#if defined(KDE_MAKE_VERSION)
# if KDE_VERSION >= KDE_MAKE_VERSION(3,1,0)
  saveMainWindowSettings (KGlobal::config (), autoSaveGroup ());
# else
  saveMainWindowSettings (KGlobal::config ());
# endif
#else
  saveMainWindowSettings (KGlobal::config ());
#endif
}

void
kradview::newToolbarConfig ()
{
  // this slot is called when user clicks "Ok" or "Apply" in the toolbar editor.
  // recreate our GUI, and re-apply the settings (e.g. "text under icons", etc.)
  createGUI ();

#if defined(KDE_MAKE_VERSION)
# if KDE_VERSION >= KDE_MAKE_VERSION(3,1,0)
  applyMainWindowSettings (KGlobal::config (), autoSaveGroup ());
# else
  applyMainWindowSettings (KGlobal::config ());
# endif
#else
  applyMainWindowSettings (KGlobal::config ());
#endif
}

void
kradview::optionsPreferences ()
{
  // popup some sort of preference dialog, here
  kradviewPreferences dlg;
  if (dlg.exec ())
    {
      // redo your settings
    }
}

void
kradview::changeStatusbar (const QString & text)
{
  // display the text on the statusbar
  statusBar ()->message (text);
}

void
kradview::changeCaption (const QString & text)
{
  // display the text on the caption
  setCaption (text);
}

void
kradview::zoomin ()
{
  m_view->m_Panel2D->zoomin ();
  if (m_view->m_Panel2D->m_zoom > 1)
    statusBar ()->message (QString ("Zoom at 1:%1").
			   arg (m_view->m_Panel2D->m_zoom));
  else
    statusBar ()->message (QString ("Zoom at %1:1").
			   arg (1 / m_view->m_Panel2D->m_zoom));

}

void
kradview::zoomout ()
{
  m_view->m_Panel2D->zoomout ();
  if (m_view->m_Panel2D->m_zoom > 1)
    statusBar ()->message (QString ("Zoom at 1:%1").
			   arg (m_view->m_Panel2D->m_zoom));
  else
    statusBar ()->message (QString ("Zoom at %1:1").
			   arg (1 / m_view->m_Panel2D->m_zoom));

}

void
kradview::nop ()
{
}

void
kradview::info ()
{
  Taglist *taglist;
  if (m_view->m_Panel2D->m_dicomimage != NULL)
    {
      taglist = new Taglist;
      taglist->calculate (m_view->m_Panel2D->m_dicomimage);
      taglist->setMouseTracking (TRUE);
      taglist->show ();
    };
}

#include "kradview.moc"
