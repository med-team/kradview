/***************************************************************************
 *   Copyright (C) 2008 by David del Rio Medina, David Santo Orcero        *
 *   ddrm@alu.uma.es, irbis@orcero.org                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DICOMIMAGE_H
#define DICOMIMAGE_H

#define _ERROR_CANTOPEN NULL
#define _ERROR_ISNOTDICOM (DICOMimage *)1
#define _ERROR_ISNOTSUPPORTED (DICOMimage *)2

#ifdef __cplusplus
extern "C"
{
#endif

#include "structDicomHead.h"

  typedef struct structPaletteColor PaletteColor;

  struct structPaletteColor
  {
    unsigned int entriesNumber;	/* not short int because maximum number of entries is 65536 */
    unsigned short firstPixelValue;
    unsigned short bitsPerEntry;
    unsigned short maxPix;
    unsigned short minPix;
    char *redTable;
    char *greenTable;
    char *blueTable;
  };

  typedef struct structDICOMimage DICOMimage;

  struct structDICOMimage
  {
    DICOMhead head;
    unsigned long int width;
    unsigned long int height;
    unsigned long int depth;
    unsigned long int size;
    unsigned long int bitsAllocated;
    unsigned short int monochromeMode;
    unsigned short int maxPix;
    unsigned short int minPix;
    char explicitHead;
    char bigEndian;
    char grayScale;
    char rgb;
    char paletteColor;
    char ybrFull;
    char jpeg;
    char rle;
    char *jpegImageLocation;
    unsigned int jpegImageSize;
    char *rleImageLocation;
    unsigned int rleImageSize;
    char withSign;
    PaletteColor paletteColorData;
    char *image;
  };



  int isDCM (const char *head);
  DICOMimage *renderDCMhead (char *buffer, char *transferSyntax,
			     long int fileSize);
  DICOMimage *loadImage (const char *filefullpath);

  char isEspecialVr (const char *vr);
  void renderMetaHeader (const char *head, char **pointerToHead,
			 char **transferSyntax);
/*	void maxMinPix ( DICOMimage *image, unsigned short int *maxPix, unsigned short int *minPix ); */
  unsigned short int uShortBigEndian (char *paux);
  unsigned short getUShort (char *buffer, char bigEndian);
  unsigned int getUInt (char *buffer, char bigEndian);
  void freeDicomImage (DICOMimage * image);

#ifdef __cplusplus
}


#endif

#endif
